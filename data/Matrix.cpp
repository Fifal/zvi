#include "Matrix.h"

Matrix::Matrix(){

}

/**
 * Returns value from index (i,j) in 1D matrix
 *
 * @brief Matrix::get_on_index
 * @param i
 * @param j
 * @param matrix
 * @param matrix_width
 * @return
 */
long Matrix::get_on_index(int i, int j, const long *matrix, int matrix_width){
    return matrix[i * matrix_width + j];
}

/**
 * Sets value on index (i,j) in 1D matrix
 *
 * @brief Matrix::set_on_index
 * @param i
 * @param j
 * @param value
 * @param matrix
 * @param matrix_width
 */
void Matrix::set_on_index(int i, int j, long value, long *matrix, int matrix_width){
    matrix[i * matrix_width + j] = value;
}

/**
 * Returns true if matrix are equal, false otherwise
 *
 * @brief Matrix::are_equal
 * @param matrix_a
 * @param matrix_b
 * @param matrix_width
 * @param matrix_height
 * @return
 */
bool Matrix::are_equal(const long *matrix_a, const long *matrix_b, int matrix_width, int matrix_height){
    for(int i = 0; i < (matrix_width * matrix_height); i++){
        if(matrix_a[i] != matrix_b[i])
            return false;
    }
    return true;
}

/**
 * Prints matrix in console
 *
 * @brief Matrix::print_matrix
 * @param matrix
 * @param matrix_width
 * @param matrix_height
 */
void Matrix::print_matrix(const long *matrix, int matrix_width, int matrix_height){
    for(int i = 0; i< matrix_height; i++){
        for(int j= 0; j < matrix_width; j++){
            std::cout << get_on_index(i, j, matrix, matrix_width);
        }
        std::cout << std::endl;
    }
}

/**
 * Creates image from matrix
 *
 * @brief Matrix::create_image
 * @param matrix
 * @param image_width
 * @param image_height
 * @param fg_color
 * @return
 */
QImage Matrix::create_image(const long *matrix, int image_width, int image_height, int fg_color){
    QImage image(image_width,image_height, QImage::Format_RGB32);

    for (int i = 0; i < image_width; ++i) {
        for (int j = 0; j < image_height; ++j) {
            QRgb color;
            if (Matrix::get_on_index(j, i, matrix, image_width) == fg_color) {
                color = qRgb(255, 255, 255);
            } else {
                color = qRgb(0, 0, 0);
            }
            image.setPixel(i, j, color);
        }
    }
    return image;
}

/**
 * Not used, creates distance transformation image, just for debugging
 *
 * @brief Matrix::create_image_dist
 * @param matrix
 * @param image_width
 * @param image_height
 * @param fg_color
 * @return
 */
QImage Matrix::create_image_dist(const long *matrix, int image_width, int image_height, int fg_color){
    QImage image(image_width,image_height, QImage::Format_RGB32);

    auto max = -1;
    for (int i = 0; i < image_width; ++i) {
        for (int j = 0; j < image_height; ++j) {
            auto current = Matrix::get_on_index(j, i, matrix, image_width);
            if(current > max){
                max = current;
            }
        }
    }

    double coef = 255.0 / max;

    for (int i = 0; i < image_width; ++i) {
        for (int j = 0; j < image_height; ++j) {
            QRgb color;
            auto clr = Matrix::get_on_index(j, i, matrix, image_width) * coef;
            color = qRgb(clr, clr, clr);
            image.setPixel(i, j, color);
        }
    }
    return image;
}
