# Semestrální práce na ZVI
Jednoduchá aplikace sloužící k vytváření skeletu z binárních obrázků pomocí mediálního osového skeletu a morfologických transformací.

![main_window](https://data.filek.cz/uploads/github-preview.png)
