#ifndef MASTHREAD_H
#define MASTHREAD_H

#include<QThread>
#include<data/Shared_data.h>
#include<data/Matrix.h>

class MASThread : public QThread {
    Q_OBJECT
private:
    // Shared data
    Shared_data* _data;

    // Image matrix
    long* _image_matrix;

    // Rerurns delta for distance trasformation
    long get_delta(int i, int j, const long *u0);

    // Returns delta for skeletonization
    long get_delta_max(int i, int j, const long *u0);

    // Retunrs 4-delta for skeletonization
    long get_four_delta_max(int i, int j, const long *u0);

    // Returns 4-delta for distance transformation
    long get_four_delta(int i, int j, const long *u0);

    // Returns 8-delta for skeletonization
    long get_eight_delta_max(int i, int j, const long *u0);

    // Returns 8-deltea for distance transformation
    long get_eight_delta(int i, int j, const long *u0);

    // Delta for distance transformation
    int _delta_distance;

    // Delta for skeletonization
    int _delta_skeleton;
public:
    MASThread(Shared_data* data);
    ~MASThread();

    void run() override;

    // Setters
    void set_delta_distance(int value);

    void set_delta_skeleton(int value);



signals:
    void finished_signal(long* result);

    void log_signal(QString data);

    void error_signal(QString data);

    void progress_signal(int progress);

    void update_image_signal(long* image);
};

#endif // MASTHREAD_H
