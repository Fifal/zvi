//
// Created by fifal on 26.2.18.
//

#include <QtGui/QImage>
#include <QtCore/QTime>
#include <QtCore/QCoreApplication>
#include "MAS.h"

MAS::MAS() {
    _delta_skeleton = 0;
    _thread = nullptr;
}

/**
* Sets Shared_data
* @param data
*/
void MAS::set_shared_data(Shared_data *data) {
    _data = data;
}

/**
* Forecfully stops working thread
*/
void MAS::stop_thread(){
    if(_thread != nullptr && _thread->isRunning()){
        _thread->terminate();
        emit log_signal(QString("Výpočetní vlákno zastaveno."));
        emit thread_progress_signal(0);
    }
}

/**
* Starts working thread
*/
void MAS::start() {
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    auto image_size = image_width * image_height;

    _matrix = new long[image_size];
    memcpy(_matrix, _data->get_image_matrix(), sizeof(long) * image_size);


    _thread = new MASThread(_data);
    _thread->set_delta_distance(_delta_distance);
    _thread->set_delta_skeleton(_delta_skeleton);

    connect(_thread, SIGNAL(finished_signal(long*)), this, SLOT(finished_slot(long*)));
    connect(_thread, SIGNAL(log_signal(QString)), this, SLOT(thread_log_slot(QString)));
    connect(_thread, SIGNAL(error_signal(QString)), this, SLOT(thread_error_slot(QString)));
    connect(_thread, SIGNAL(progress_signal(int)), this, SLOT(thread_progress_slot(int)));
    connect(_thread, SIGNAL(update_image_signal(long*)), this, SLOT(update_image_slot(long*)));

    _thread->start();
    if(_thread->isFinished()){
        delete(_thread);
    }
}

/**
* Slot which updates image on GUI
* @param image
*/
void MAS::update_image_slot(long* image){
    _image = Matrix::create_image(image, _data->get_image_width(), _data->get_image_height(), _data->get_fg_color());
    emit image_signal(_image);
}

/**
* Slot which updates image on GUI and resets progress bar to zero
* @param image
*/
void MAS::finished_slot(long* image){
    _image = Matrix::create_image(image, _data->get_image_width(), _data->get_image_height(), _data->get_fg_color());
    emit image_signal(_image);
    emit thread_progress_signal(0);
    delete(image);
}

/**
* Loggs from thread to GUI console
* @param data
*/
void MAS::thread_log_slot(QString data){
    emit log_signal(data);
}

/**
* Loggs error from thread to GUI console
* @param data
*/
void MAS::thread_error_slot(QString data){
    emit error_signal(data);
}

/**
* Updates progressbar
* @param progress
*/
void MAS::thread_progress_slot(int progress){
    int value = ((double)progress / (double)_data->get_max_iters()) * 100;
    emit thread_progress_signal(value);
}

/**
* Returns result image
*/
QImage MAS::get_image() {
    return _image;
}

/**
* Sets delta for distance transformation
* @param value
*/
void MAS::set_delta_distance(int value){
    _delta_distance = value;
}

/**
* Sets delta for skeletonization
* @param value
*/
void MAS::set_delta_skeleton(int value){
    _delta_skeleton = value;
}
