#include "algorithms/MASThread.h"

MASThread::MASThread(Shared_data* data)
{
    _data = data;
}

MASThread::~MASThread(){
    delete(_image_matrix);
}

void MASThread::run(){
    emit log_signal(QString("Spuštěno výpočetní vlákno mediální osové skeletizace."));
    if(_delta_distance == 0)
        emit log_signal(QString("Je použito 4-okolí pro vzdálenostní transformaci."));
    else
        emit log_signal(QString("Je použito 8-okolí pro vzdálenostní transformaci."));

    if(_delta_skeleton == 0)
        emit log_signal(QString("Je použito 4-okolí pro skeletonizaci."));
    else
        emit log_signal(QString("Je použito 8-okolí pro vzdálenostní."));

    emit log_signal(QString("Používá se práh: ")+ QString::number(_data->get_threshold()));

    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    auto image_size = image_width * image_height;
    auto max_iters = _data->get_max_iters();

    long *u0 = new long [image_size];
    memcpy(u0, _data->get_image_matrix(), sizeof(long) * image_size);

    _image_matrix = new long [image_size];
    memcpy(_image_matrix, u0, sizeof(long) * image_size);

    long *u_iter = new long[image_size];

    int count = 1;
    while (true) {
        // Nakopirovani puvodni matice do matice iterace
        memcpy(u_iter, u0, sizeof(long) * image_size);

        // Pro kazdy bod spocitat vzdalenostni trasformaci, bud 4 nebo 8 okoli get_delta
        for (int i = 0 + count; i < image_width - count; i++) {
            for (int j = 0 + count; j < image_height - count; j++) {

                // Matice je 1D pole, pro vetsi prehlednost pristup do matice jako do 2D pole pres metody set_on_index a get_on_index
                Matrix::set_on_index(j, i, get_delta(j, i, u0), u_iter, image_width);
            }
        }

        count++;
        // Pokud jsou matice stejne nebo maximalni pocet pruchodu -> konec cyklu
        if (Matrix::are_equal(u0, u_iter, image_width, image_height) || count >= max_iters) {
            break;
        }

        // Nakopirovani matice iterace do u0, v u0 je pokazde predchozi iterace
        memcpy(u0, u_iter, sizeof(long) * image_size);

        // Kazdy 10 pruchod aktualizace progressbaru v GUI
        if(count %10 == 0){
            emit progress_signal(count);
        }
    }


    // Matice skeletu
    long *skelet = new long [image_size];
    memset(skelet,0,sizeof(long)*image_size);

    // Prochazeni matice vzdalenostni trasformace
    for (int k = 0; k < image_width-1; k++) {
        for (int i = 0; i < image_height-1; i++) {
            // Bod na pozici (i, k)
            long u = Matrix::get_on_index(i, k, u0, image_width);

            // Pokud je hodnota bodu 0 => pozadi
            if (u == 0) {
                Matrix::set_on_index(i, k, 0, skelet, image_width);
                continue;
            }

            // Pokud je u maximalni v danem okoli => patri do skeletu
            if (u >= get_delta_max(i, k, u0)) {
                Matrix::set_on_index(i, k, 1, skelet, image_width);
            } else {
                Matrix::set_on_index(i, k, 0, skelet, image_width);
            }
        }
    }
    emit finished_signal(skelet);
    delete(u0);
    delete(_image_matrix);

}

/**
 * Sets _delta_distance
 *
 * @brief MASThread::set_delta_distance
 * @param value
 */
void MASThread::set_delta_distance(int value){
    _delta_distance = value;
}

/**
 *  Sets _delta_skeleton
 *
 * @brief MASThread::set_delta_skeleton
 * @param value
 */
void MASThread::set_delta_skeleton(int value){
    _delta_skeleton = value;
}

/**
 * Returns distance transformation for given point in 4 or 8-delta based on _delta_distance value
 *
 * @brief MASThread::get_four_delta
 * @param i x pos
 * @param j y pos
 * @param u0 image matrix
 * @return long
 */
long MASThread::get_delta(int i, int j, const long *u0){

    // 4-delta
    if(_delta_distance == 0){
        return get_four_delta(i,j,u0);
    }else{
        return get_eight_delta(i,j,u0);
    }
}

/**
 * Returns if point (i,j) is skeleton or not in 4 or 8-delta based on _delta_skeleton value
 *
 * @brief MASThread::get_delta_max
 * @param i
 * @param j
 * @param u0
 * @return
 */
long MASThread::get_delta_max(int i, int j, const long *u0){

    // 4-delta
    if(_delta_skeleton == 0){
        return get_four_delta_max(i,j,u0);
    }else{
        return get_eight_delta_max(i,j,u0);
    }
}

/**
 * Returns distance transformation for given point in 4-delta
 *
 * @brief MASThread::get_four_delta
 * @param i x pos
 * @param j y pos
 * @param u0 image matrix
 * @return long
 */
long MASThread::get_four_delta(int i, int j, const long *u0) {
    auto image_width = _data->get_image_width();

    long point_u = Matrix::get_on_index(i, j, u0, image_width);

    long point_up = Matrix::get_on_index(i - 1, j, u0, image_width);
    long point_down = Matrix::get_on_index(i, j - 1, u0, image_width);
    long point_left = Matrix::get_on_index(i, j + 1, u0, image_width);
    long point_right = Matrix::get_on_index(i + 1, j, u0, image_width);

    long min = std::min({point_up, point_down, point_left, point_right, point_u});

    return Matrix::get_on_index(i, j, _image_matrix, image_width) + min;
}

/**
 * Returns if point (i,j) is skeleton or not in 4-delta
 *
 * @brief MASThread::get_four_delta
 * @param i x pos
 * @param j y pos
 * @param u0 image matrix
 * @return long
 */
long MASThread::get_four_delta_max(int i, int j, const long *u0) {
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();

    long point_up = (j - 1) < 0 ? 0 : Matrix::get_on_index(i, j - 1, u0, image_width);
    long point_down = (j + 1) > image_width ? 0 : Matrix::get_on_index(i, j + 1, u0, image_width);
    long point_left = (i - 1) < 0 ? 0 : Matrix::get_on_index(i - 1, j, u0, image_width);
    long point_right = (i + 1) >image_height ? 0 : Matrix::get_on_index(i + 1, j, u0, image_width);

    long max = std::max({point_up, point_down, point_left, point_right});

    return max;
}

/**
 * Returns distance transformation for given point in 8-delta
 *
 * @brief MASThread::get_four_delta
 * @param i x pos
 * @param j y pos
 * @param u0 image matrix
 * @return long
 */
long MASThread::get_eight_delta(int i, int j, const long *u0) {
    auto image_width = _data->get_image_width();

    long point_u = Matrix::get_on_index(i, j, u0, image_width);

    long point_up = Matrix::get_on_index(i - 1, j, u0, image_width);
    long point_up_right = Matrix::get_on_index(i - 1, j + 1, u0, image_width);
    long point_up_left = Matrix::get_on_index(i - 1, j - 1, u0, image_width);
    long point_down = Matrix::get_on_index(i + 1, j, u0, image_width);
    long point_down_right = Matrix::get_on_index(i + 1, j + 1, u0, image_width);
    long point_down_left = Matrix::get_on_index(i + 1, j - 1, u0, image_width);
    long point_left = Matrix::get_on_index(i - 1, j, u0, image_width);
    long point_right = Matrix::get_on_index(i + 1, j, u0, image_width);

    long min = std::min({point_up, point_down, point_left, point_right, point_u,
                         point_down_right, point_down_left, point_up_left, point_up_right});

    return Matrix::get_on_index(i, j, _image_matrix, image_width) + min;
}

/**
 * Returns if point (i,j) is skeleton or not in 8-delta
 *
 * @brief MASThread::get_four_delta
 * @param i x pos
 * @param j y pos
 * @param u0 image matrix
 * @return long
 */
long MASThread::get_eight_delta_max(int i, int j, const long *u0) {
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();

    long point_up = (i - 1) < 0 ? 0 : Matrix::get_on_index(i-1, j, u0, image_width);
    long point_up_right = (j + 1) > image_width || (i-1) < 0 ? 0 : Matrix::get_on_index(i - 1, j + 1, u0, image_width);
    long point_up_left = (j - 1) < 0 || (i - 1) <0 ? 0 : Matrix::get_on_index(i - 1, j - 1, u0, image_width);
    long point_down = (i + 1) > image_height ? 0 : Matrix::get_on_index(i + 1, j, u0, image_width);
    long point_down_right = (j + 1) > image_width || (i+1) > image_height ? 0 : Matrix::get_on_index(i + 1, j + 1, u0, image_width);
    long point_down_left = (j - 1) < 0 || (i+1) > image_height ? 0 : Matrix::get_on_index(i + 1, j - 1, u0, image_width);
    long point_left = (i - 1) < 0 ? 0 : Matrix::get_on_index(i - 1, j, u0, image_width);
    long point_right = (i + 1) > image_height ? 0 : Matrix::get_on_index(i + 1, j, u0, image_width);

    long max = std::max({point_up, point_down, point_left, point_right,
                         point_down_right, point_down_left, point_up_left, point_up_right});

    return max;
}
