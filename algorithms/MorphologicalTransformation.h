#ifndef MORPHOLOGICALTRANSFORMATION_H
#define MORPHOLOGICALTRANSFORMATION_H

#include<cstdint>
#include<iostream>
#include<fstream>
#include<bitset>
#include<QImage>
#include<QWidget>
#include<data/Shared_data.h>
#include<algorithms/MorphThread.h>
#include<data/Matrix.h>

class MorphologicalTransformation : public QObject{
    Q_OBJECT
private:
    Shared_data* _data;
    int _golay;

    void print_byte(uint16_t byte);

    QImage _image;

    MorphThread* _thread;

public:
    MorphologicalTransformation();

    void start();

    void set_shared_data(Shared_data* data);

    void set_golay(int golay);

    QImage get_image();

    void stop_thread();

signals:
    void log_signal(QString data);

    void error_signal(QString data);

    void image_signal(QImage image);

    void thread_progress_signal(int progress);

private slots:

    void finished_slot(long* result);

    void thread_log_slot(QString data);

    void thread_error_slot(QString data);

    void thread_progress_slot(int progress);

    void update_image_slot(long* image);
};

#endif // MORPHOLOGICALTRANSFORMATION_H
