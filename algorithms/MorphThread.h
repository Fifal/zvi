#ifndef MORPHTHREAD_H
#define MORPHTHREAD_H

#include<QThread>
#include<data/Shared_data.h>
#include<data/Matrix.h>
#include<QTime>
#include<QCoreApplication>
#include<QEventLoop>

class MorphThread : public QThread
{
    Q_OBJECT
private:
    Shared_data* _data;

    // value 0 = even
    // value 1 = odds
    // value 2 = All of them
    int _golay;
    /*
     * 9 8 7
     * 6 5 4
     * 3 2 1
     */

    // Masks from Golay alphabet
    uint16_t _l11 = 23; // L3
    uint16_t _l12 = 448;

    uint16_t _l21 = 50;
    uint16_t _l22 = 200;

    uint16_t _l31 = 308; // L5
    uint16_t _l32 = 73;

    uint16_t _l41 = 176;
    uint16_t _l42 = 11;

    uint16_t _l51 = 464; // L2
    uint16_t _l52 = 7;

    uint16_t _l61 = 152;
    uint16_t _l62 = 38;

    uint16_t _l71 = 89; // L1
    uint16_t _l72 = 292;

    uint16_t _l81 = 26;
    uint16_t _l82 = 416;


    // Even masks
    uint16_t _masks_even[8] = {_l11, _l12,
                               _l31, _l32,
                               _l51, _l52,
                               _l71, _l72};

    // Odd masks
    uint16_t _masks_odd[8] = {_l21, _l22,
                              _l41, _l42,
                              _l61, _l62,
                              _l81, _l82};

    // All masks
    uint16_t _masks_all[16] = {_l11, _l12,
                               _l21, _l22,
                               _l31, _l32,
                               _l41, _l42,
                               _l51, _l52,
                               _l61, _l62,
                               _l71, _l72,
                               _l81, _l82};


    // Format is
    //
    // 9 8 7
    // 6 5 4
    // 3 2 1
    //
    // Returns surrounding in one number
    uint16_t get_surrounding(int i, int j,const long* matrix);

    // Returns erosion
    long* erosion(long* a, uint16_t mask);
    // Returns complement
    long* complement(long* a);
    // Returns intersection
    long* intersection(long* a, long* b);
    // Returns thinning
    long* thinning(long* a, uint16_t mask, uint16_t mask2);

public:
    MorphThread(Shared_data* data);

    // Starts thread
    void run() override;
    // Sets golay, 0 -> even masks, 1 -> odd masks, 2 -> all masks
    void set_golay(int golay);

signals:
    void finished_signal(long* result);
    void log_signal(QString data);
    void error_signal(QString data);
    void progress_signal(int progress);
    void update_image_signal(long*);
};

#endif // MORPHTHREAD_H
