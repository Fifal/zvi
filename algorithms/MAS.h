//
// Created by fifal on 26.2.18.
//

#ifndef ZVI_MAS_H
#define ZVI_MAS_H

#include <data/Shared_data.h>
#include <QWidget>
#include "MASThread.h"

/**
 * Medial Axis Skletionazation settings class
 */

class MAS : public QObject {
    Q_OBJECT
private:
    // Shared data
    Shared_data *_data;

    // Image matrix
    long *_matrix;
    
    // Result image
    QImage _image;

    // Delta for distance transformation 
    int _delta_distance;
    // Delta for skeletonization
    int _delta_skeleton;

    // Working thread with MAS
    MASThread* _thread;

public:
    MAS();

    // Sets shared data
    void set_shared_data(Shared_data *data);

    // Starts new thread with MAS algorithm
    void start();

    // Returns result image
    QImage get_image();

    // value 0 = 4-delta
    // value 1 = 8-delta
    void set_delta_distance(int value);

    // value 0 = 4-delta
    // value 1 = 8-delta
    void set_delta_skeleton(int value);

    // Force stop running thread
    void stop_thread();

signals:

    void log_signal(QString data);

    void error_signal(QString data);

    void image_signal(QImage image);

    void thread_progress_signal(int progress);

private slots:

    void finished_slot(long* result);

    void thread_log_slot(QString data);

    void thread_error_slot(QString data);

    void thread_progress_slot(int progress);

    void update_image_slot(long* image);

};


#endif //ZVI_MAS_H
