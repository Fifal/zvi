#ifndef PICTUREPREVIEW_H
#define PICTUREPREVIEW_H

#include <QWidget>
#include <QGraphicsView>
#include <QSlider>

namespace Ui {
class PicturePreview;
}

class PicturePreview : public QWidget
{
    Q_OBJECT

public:
    explicit PicturePreview(QWidget *parent = 0);
    ~PicturePreview();

    void set_image(QImage image);

signals:
    void on_close_signal();

private:
    QImage _image;
    Ui::PicturePreview *ui;
    void closeEvent(QCloseEvent *bar) override;

    QGraphicsView * _gw;
    QSlider * _slider;

private slots:
    void value_changed_slot(int value);

};

#endif // PICTUREPREVIEW_H
